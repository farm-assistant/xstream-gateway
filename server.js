"use strict";


const logger           = require("./lib/xutils.js").logger;
const ARQ              = require('./lib/xarq').ARQ;
const RxGram           = require('./lib/xarq').RxGram;
const xStreamManager   = require('./lib/xstream-manager').xStreamManager;
const xBeeStream       = require('./lib/xstream').xBeeStream;
const SerialPort       = require('serialport');
const xbee_api         = require('xbee-api');
const C                = xbee_api.constants;

const FLAGS_BUFFERED   = 0x1; // Buffered Stream


/**
 * Open the serial connection to the XBee modem
 * On request from an XBee device, establish a
 * stream using Unix sockets.
 * 
 **/

const warning =  '\n\n\nxbee-unix-stream  Copyright (C) 2015  Andrew McCure <andrew@agsense.co.nz>\n\n' + 
                'This software comes with ABSOLUTELY NO WARRANTY.\n' + 
                'This is free software, and you are welcome ' +
                'to redistribute it under certain conditions.\n\n' + 
                'For commercial use, please contact the author for a license.\n\n\n';
                

const run = function (options) {

    var $this = this;
    var serialport;
    var xstream;
    
    options.baud  	      = options.baud       || 115200;
    options.delay 	      = options.delay      || 500;
    options.socketPath 	 = options.socketPath || "/tmp/sockets";

    
    if (!options.hasOwnProperty("port")){
        throw new Exception("No serial port specified");
    }

    var xbeeAPI = new xbee_api.XBeeAPI({
      api_mode: 2,
      raw_frames:false
    });

    try {
        serialport = new SerialPort(options.port, {
          baudrate: options.baud,
          buffersize:512,
          rtscts: false,
          parser: xbeeAPI.rawParser()
        });

        xstream = new xBeeStream(xbeeAPI,serialport);
        serialport.on('open',()=>{
		
            logger.log("Opened ", options.port); 
            $this.streams = {};
            $this.nostreams = function() { return Object.keys($this.streams).length ==0;}
            xstream.purgeSockets(options.socketPath); // purge old sockets
            xstream.helo(); // Send PING once on startup
        });

        serialport.on('end',()=>{
            $this._cleanup("Serial Port End");
        });

        serialport.on('error',()=>{
            $this._cleanup("Serial Port Error");
        });

        this._reset = function (e = "") {
           xstream.rset();
           xstream.localATCmd("FR");
        };
        
        this._cleanup = function (e = "") {
            process.removeListener('SIGINT',$this._cleanup)
            process.removeListener('SIGTERM',$this._cleanup)
            for (var key in $this.streams){
                $this.streams[key].emit('end');
            }
            xstream.purgeSockets(options.socketPath); // purge old sockets
            logger.log("Exiting",e);
            process.exit(0);
        }


    } catch (e) {
        logger.error("Error on initialisation: " + e.toString());
        process.exit(1);
    }    

    try{
        xbeeAPI.on("frame_object", function(_frame) {
    	    //logger.info("Got Frame",_frame);
            if (_frame.type == C.FRAME_TYPE.AT_COMMAND_RESPONSE){ // Doesn't work?
                logger.info("Local procedure response");
                logger.info(_frame.commandData.toString().trim());
                     
                return;
            }

            if (_frame.type == C.FRAME_TYPE.REMOTE_COMMAND_RESPONSE){ // Doesn't work?
                var address64  = _frame.remote64;
                logger.info("Remote procedure response");
                if (_frame.command == "NI" && $this.streams.hasOwnProperty(address64)){
                     var ni = _frame.commandData.toString().trim();
                     $this.streams[address64].link(ni); // link name to socket
                     $this.streams[address64].linked = true;
                }
                return;
            }

            if (_frame.type == C.FRAME_TYPE.ZIGBEE_RECEIVE_PACKET) {
                    // check dest address
                var address64  = _frame.remote64;
  
                if (!$this.streams.hasOwnProperty(address64)) {
                // Create UNIX pipe
                    var rxGram = new RxGram(_frame.data);
                    if (!rxGram.valid()){
                        logger.warn("Invalid packet");
                        return;
                    }
                    
                    options['buffered'] = rxGram.flag(FLAGS_BUFFERED);

                    $this.streams[address64] = new xStreamManager(xstream,
                                                            new ARQ(options),
                                                            address64,
                                                            options);
                    
                    $this.streams[address64].on('end',function() { 
                    logger.info("Deleting " + address64);
                            delete $this.streams[address64];
                    });

                    $this.streams[address64].linked       = false;
                    $this.streams[address64].linkAttempts = 5; 
                    xstream.remoteATCmd("NI",address64);

                    $this.linkCheckerTimeout = setInterval(()=>{
                        $this.streams[address64].linkAttempts--;
                        if ($this.streams[address64].linked ||
                            $this.streams[address64].linkAttempts ==0) {
                            clearTimeout($this.linkCheckerTimeout);
                            return;
                        }
                        xstream.remoteATCmd("NI",address64);
                    },1000); // Request name for linking
                     
                }
                xstream.emit(address64,_frame.data);
            }
        });
        xbeeAPI.on("error", function(e) {
            if (!e.toString().indexOf("Checksum") > 0)
                throw(e);
            $this._cleanup(e);
        });
    } catch(e){
        logger.error(e.stack);
    } finally{
        logger.info("Exiting");
        process.on('SIGINT',$this._cleanup);
        process.on('SIGTERM',$this._cleanup);
        process.on('SIGHUP',$this._reset); // Send a network reset
    }
}    


module.exports.run = run;
