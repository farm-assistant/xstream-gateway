# Introduction

X-Stream

Networking Protocol and Utilities to communication with AgSense's Farm-Assistant PCBs 

Includes:

Server process that utilises Serial Port, connects to gateway devices, and maps incoming REUs to UNIX sockets

(See config/config.yaml)

Includes client libraries:

Simple stream to handle encapsulation and deencapsulation of xstream packets.

XStream packets allow the combining of packets over a serial stream where individual transmissions are size bound

Installation:

npm install --unsafe-perm
