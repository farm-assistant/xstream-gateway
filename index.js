var XStreamEncode = require('./lib/encode');
var XStreamDecode = require('./lib/decode');
var XStreamAPI    = require('./lib/api');

module.exports = {
	Encode : XStreamEncode,
	encode : XStreamEncode,
	Decode : XStreamDecode,
	decode : XStreamDecode,
	API : XStreamAPI,
	api : XStreamAPI
};
