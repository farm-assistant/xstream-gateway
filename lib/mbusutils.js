var util = require('util');
var Events = require('events').EventEmitter;
var PDU = require ('modbus-pdu');
var crc  = require('crc');

/**
 * Assume REUs are Modbus Slave/Masters
 *
 *
 * Todo: Create Master <-> Slave (Buffer) <-> Master mode
 *
 * 
 **/

function ModbusConverter(){
    this.transactionId = 0;
    var $this = this;
    
    this.fromTCP = function (buffer) {
        $this.transactionId = buffer.readUInt16LE(0);
        var data = Buffer.concat([buffer.slice(6),new Buffer(2)]);;
        
        var _crc = crc.crc16modbus(data.slice(0,data.length-2));
        data.writeUInt16LE(_crc,data.length-2);
        return data;
    }
    
    // Add transaction identifier
    this.toTCP =  function (buffer){
       var data = new Buffer(buffer.length + 6);
       data.writeUInt16LE($this.transactionId, 0);
    
       data.writeUInt16BE(0, 2);
       data.writeUInt16BE(buffer.length, 4);
    			data.writeUInt8(1,6);
    
       buffer.copy(data, 6);
       return data;
    }
}

function modbusWrapper(socketIn,config){
    Events.call(this);
    $this = this;
    _socketIn = socketIn;
    _host = config.host;
    _port = config.port;

    console.log("New socket");
}
    

function modbusSlaveProxy(socketIn,config){
    modbusWrapper.call(this,socketIn,config);

    _cleanup = function (){
        _socketIn.destroy();
        _socketIn.destroy();
        $this.emit('end')
    }

    _socketIn.on('close',function(){
        console.log("Closing");
        _cleanup();
     });
    
    var _socketOut = net.createConnection(_port,_host);
        
    this.mc = new ModbusConverter();
    

    _socketOut.on('data',function(data){
        var b = new Buffer(data);
        var _o = $this.mc.fromTCP(b);
        console.log(PDU.Response(_o.slice(1,_o.length-2)));
        _socketIn.write(_o.slice(0,_o.length-2));
     });

    _socketIn.on('data',function(data){
     var b  =  new Buffer(data);
     console.log(PDU.Request(b.slice(1,b.length-2)));
     
     $this.mc.transactionId = ($this.mc.transactionId +1) % 255;
     _socketOut.write($this.mc.toTCP(b));
    });
}


function modbusMasterProxy(socketIn,config){
    modbusWrapper.call(this,socketIn,config);
    this.waitData = 500;//ms
    
    
    _cleanup = function (){
        _socketIn.destroy();
        $this.server.close();
        $this.emit('end')
    }

    _socketIn.on('close',function(){
        console.log("Closing");
        _cleanup();
     });

    this.server = net.createServer(function(_socketOut) {
       this.maxConnections =1;
       console.log("CONNECT");
       var timeout = 0;
       var buffer = new Buffer([]);

       _socketOut.mc = new ModbusConverter();
        _writeOut = function (data) {
                var b  =  new Buffer(data);
                console.log("Got");
                console.log(b);
                try{
                    console.log(PDU.Response(b.slice(1,b.length-2)));
                    _socketOut.write(_socketOut.mc.toTCP(b.slice(0,b.length-2))); //hmmm change toTCP to remove those last pesky 2 bytes.  Needs to work with master!
                } catch(e) {
                    console.error(e);
                    _cleanup();
                }
        };

        _writeBuffer = function (data) {
            buffer = Buffer.concat([buffer,new Buffer(data)]);
            console.log("data! " + data.length);
            clearTimeout(timeout);
            timeout = setTimeout(function (){
                        _writeOut(buffer);
                        buffer= new Buffer([]);
                        },$this.waitData);
        };
       _socketIn.on('data',_writeBuffer);
       
        _socketOut.on('data',function(data){
             var b  =  _socketOut.mc.fromTCP(new Buffer(data));
            try{
                console.log(PDU.Request(b.slice(1,b.length-2)));
                _socketIn.write(b);
                } catch(e) {
                    console.error(e);
                    _cleanup();
                }
             console.log(b);
            });
        
        _socketOut.on('end',function(){
             console.log("END");
            _socketIn.removeListener('data',_writeBuffer);
        });
        
        }).listen(_port,_host)
            .on('connect',function() { console.log("Server listening on " + _host + ":" + _port);})
            .on('error',function(err) {
                 console.error(err);
                 _cleanup();
       });
}

modbusWrapper.prototype = Object.create(Events.prototype);
modbusSlaveProxy.prototype = Object.create(modbusWrapper.prototype);
modbusMasterProxy.prototype = Object.create(modbusWrapper.prototype);

module.exports.modbusMasterProxy = modbusMasterProxy;
module.exports.modbusSlaveProxy  = modbusSlaveProxy;
