"use strict";


/**
 * XARQ
 *
 * Implements a Selective Repeat Serial protocols
 * Protocol is symmetrical; binary; and reliable
 *
 * TODO:
 * Add regular ping and exit and timeout!!!
 * Rename logger.js to xutils.js and add check for valid timer
 *
 **/

const xutils = require("./xutils.js"); 
const logger = require("./xutils.js").logger;
const util = require('util');
const Events = require('events').EventEmitter;
const crc  =require('crc');


const TEST = false;
const MAX_TXNQ_LENGTH = 500

const DG_HEADER   =   6;
const DG_PAYLOAD  =   96;
const DG_SIZE = DG_HEADER + DG_PAYLOAD;

// Header constants
const DG_CRC           = 0; // Checksum
const DG_WID           = 1; // WinId (use high and low 4 bits separately)
const DG_MASK          = 2; // Ack Mask
const DG_CNTRL_PARAM   = 2;
const DG_CNTRL_PARAM_2 = 3;
const DG_ACKED_MASK    = 3; // Acked Mask
const DG_LEN           = 4; // Data Length
const DG_ATTEMPT       = 5; // Resend Attempt
const DG_FLAGS         = 5; // Flags - ie. is data encoded


// Control Frame Types
const F_CNTRL         = 0xF;
const F_PING          = 0x1;
const F_PONG          = 0x2;
const F_SLEEP         = 0x3;

// Flags
const  FLAGS_BUFFERED  = 0x1 <<0;
const  FLAGS_IS_CLIENT = 0x1 <<1;



// ARQ Constants
const ARQ_MAX     	= 14;
const ARQ_WMAX    	= ARQ_MAX / 2;
const ARQ_UNPUNG  	= 0;
const ARQ_SENDNEW 	= 1;
const ARQ_REQACK  	= 2;
const ARQ_RESEND  	= 3;
const ARQ_ACKED   	= 4;
const ARQ_IDLE   	= 400; // ms of idle time before queuing remaining input data
const ARQ_ENCODED	= 0x1; // Buffer is encoded
const ARQ_RESET		= 0x1;
const ARQ_MAXRETRY	= 0x0F;
const ARQ_TIMEOUT   = 30000; // 30 sconds


// DATA FRAME [CRC,AckWinId <<4+ WinId, Mask,  AckMask, Len, Data ...  ]
// CNTL FRAME [CRC,CNTLID <<4 +FUNC, Param1, Param2, Len,  Data ... ]

function DataGram(){
    
    Events.call(this);
    var $this = this;
    this._sent = false;
    this._recv = false;
    this._data = null        
    this._frameId = 0;
    this._ack = false;
    this._len =0;
    
    // returns first set bit from right
    // or -1 if no bits set

    this._getLastSetBit = function (mask) {
        // Assert mask !=0?
        if (mask == 0)
            return -1;
        var offset = 8-ARQ_WMAX;
        while( (mask & 0x1 << offset) == 0  && offset <= ARQ_WMAX)
            offset ++;
        return ARQ_WMAX - offset;
    };

    // returns first set bit from left
    // or -1 if no bits set
    this._getFirstSetBit = function (mask) {
        if (mask == 0)
            return -1;
        var offset = 0;
        while(mask >> (8 - offset) & 0x1 == 0  && offset <8)  
            offset ++;
        return offset;
    };

    this._setWidLo = function (val) {
        $this._data[DG_WID] = ($this._data[DG_WID] & 0xF0) + val;
    };

    this._getWidLo = function () {
        return $this._data[DG_WID] & 0x0F;
    };

    this._setWidHi = function (val) {
        $this._data[DG_WID] = ($this._data[DG_WID] & 0x0F) + (val << 4);
    };

    this._getWidHi = function () {
        return $this._data[DG_WID] >> 4;
    };

    this.setAttempt = function (attempt) {
        $this._data[DG_FLAGS] = $this._data[DG_ATTEMPT] | (attempt << 4);
    };

    this.setFlag = function (flag) {
        $this._data[DG_FLAGS] = $this._data[DG_FLAGS] & flag;
    };
    
    this.setControlParam = function (val) {
        $this._data[DG_CNTRL_PARAM] =  val;
    };

    this.setControlParam2 = function (val) {
        $this._data[DG_CNTRL_PARAM_2] =  val;
    };

    this.getControlParam = function () {
        return $this._data[DG_CNTRL_PARAM];
    };

    this.getControlParam2 = function () {
        return $this._data[DG_CNTRL_PARAM_2];
    };

    this.isRecv = function () {
        return $this._recv;
    };
    
    this.setRecv = function (state) {
        $this._recv = state;
    };

    this.isSent = function () {
        return $this._sent;
    };
    
    this.setSent = function (state) {
        $this._sent = state;
    };
    
    this.setWinId = function (winId) {
       // $this._data[DG_WID] = winId;
       $this._setWidLo(winId);
    };
    
    this.getWinId = function (offset) {
         return  $this._getWidLo();
    };

    this.getWinOffset = function () {
        return $this._getLastSetBit($this._data[DG_MASK] ^ 0x1); // convert to 0 offset
    };

    this.getFrameId = function () {
        return $this.getWinId() + $this.getWinOffset();
    };

    this.setAckedWinId = function (winId) {
        $this._setWidHi(winId);
    };
    
    this.getAckedWinId = function () {
        return $this._getWidHi();
    };

    this.getAckedWinIdSize = function () {
        return $this.getAckedWinId() + $this.getAckedWinOffset();
    };

    this.ackRequested = function () {
        return $this._data[DG_MASK] & 0x1;
    };

    this.ackReceived = function () {
        return $this._data[DG_ACKED_MASK] > 0;
    };

    this.getMask = function () {
        return $this._data[DG_MASK] ^ 0x1;
    };

    this.getAckedMask = function () {
        return $this._data[DG_ACKED_MASK] ^ 0x1;
    };
    
    this.payload = function () {
        return $this._data.slice(DG_HEADER,DG_HEADER+$this.getDataLength());
    };

    this.getDataLength = function () {
        return  $this._data[DG_LEN];
    };

    this.setDataLength = function (len){    
            $this._data[DG_LEN] = len; // Length of frame
    };
    

    this._pad = function (length){
        for(var i=DG_HEADER + $this._data[DG_LEN];
            i <  length;
            i++) {
                $this._data[i] = 0x0; // Math.round(Math.random() * 0xFF);
        }
    };

    this.calcCRC8 = function(length){
        $this._pad(length);
        //$this._rand(); // replace with flag
        $this._data[DG_CRC] = crc.crc8($this._data.slice(1,length));
    };

    this.attempt = function(length){
        return $this._data[DG_ATTEMPT] & 0xF0;
    };

    this.flag = function(length){
        return $this._data[DG_FLAGS] & 0x0F;
    };

    this.valid = function(length){
        return $this._data[DG_CRC] == crc.crc8($this._data.slice(1,length));
    };

    this.frame = function(){
      return $this._data;  
    };

    this.isControlFrame = function () {
        return this._getWidHi() == F_CNTRL;
    };
}


function RxGram(data){
    Events.call(this);
    DataGram.call(this);
    var $this = this;
    this._data = new Buffer(data);

    this.isAckReceived = function(offset){
         return $this._data[DG_ACKED_MASK] >> (ARQ_WMAX-offset) & 0x1;
    }

    this.hasData = function(){
        return $this.getWinOffset() >=0 && $this.getWinOffset() <= ARQ_WMAX && $this.getDataLength() >0;
    }
}


function TxGram(data,end){
        DataGram.call(this);
        var $this = this;

        this._len = data.length+DG_HEADER; // + header length
        this._data = new Buffer(DG_HEADER + DG_PAYLOAD);
        this._end = end;
        this._attempts = 0;
        this._acked = false;
        this._data[DG_MASK] = 0x0;
        this._data[DG_ACKED_MASK] = 0x0;
        
        data.copy(this._data,DG_HEADER);

        this.setWinId(0);   
        this.setAckedWinId(0);
        this.setDataLength(data.length);
        
        this.isEnd = function (){
            return $this._end;
        }

        this.setPayload = function(data) {
         if (data.length > DG_PAYLOAD)
          return;
         data.copy($this._data,DG_HEADER);
         $this.setDataLength(data.length);
        }

        this.incrAttempts = function(){
            $this._attempts = Math.min(ARQ_MAXRETRY,$this._attempts +1);
        }
        
        this.attempts = function (){
            return $this._attempts;
        }
            
        this.setMask = function (offset,ack){
            // assert offset < ARQ_WMAX
            if (offset >=0 && offset < ARQ_WMAX)
                $this._data[DG_MASK]  = $this._data[DG_MASK] | 0x1 << (ARQ_WMAX-offset);
            $this._data[DG_MASK] = (ack) ?
                $this._data[DG_MASK] | 0x1 :
                $this._data[DG_MASK] & 0xFE;
        };

        this.setAckedMask = function (offset,ack){
            // assert offset < ARQ_WMAX
            if (offset >=0 && offset < ARQ_WMAX)
                $this._data[DG_ACKED_MASK]  = this._data[DG_ACKED_MASK] | (0x1 << (ARQ_WMAX-offset));
            if (ack)
               $this._data[DG_ACKED_MASK] = this._data[DG_ACKED_MASK] | 0x1;
        };
        
        this.isAcked = function(offset){
             return  $this._data[DG_MASK] & (0x1 << (ARQ_WMAX-offset));
        };

        this.mergeAcked = function (txGram){
            $this.setAckedMask(txGram.getAckedMask());  
            $this.setAckedWinId(txGram.getAckedWinId());
        };

        this.unmergeAcked = function (){
            $this._data[DG_ACKED_MASK] = 0x0;
            $this.setAckedWinId(0);
        };

};
 

DataGram.prototype = Object.create(Events.prototype);
TxGram.prototype   = Object.create(DataGram.prototype);
RxGram.prototype   = Object.create(DataGram.prototype);

if (typeof Array.prototype.peek!= 'function') {
    Array.prototype.peek = function(){
     return this.length > 0 ? this[0] : null;
    }
}

if (typeof String.prototype.isEqual!= 'function') {
    String.prototype.isEqual = function (str){
        return this.toUpperCase()==str.toUpperCase();
     };
}

ARQ.prototype = Object.create(Events.prototype);

function ARQ(options){
    Events.call(this);
    var txq = [];

    var $this       = this;
    this.options    = options;
    this.tickInterval = 115200/options.baud*options.delay;
    this.options.pingTimeout = options.pingTimeout | 60000;
    var timeout;
    if (options.buffered)
    	this.flags 	    = FLAGS_BUFFERED;

    this.loop       = 0;

    this.txOffset   = 0;
    this.txStart    = 0; // First frame Id of tx Window being sent
    this.txState    = ARQ_UNPUNG;
    this.txAcked    = false;
    this.txSize     = 0;
    this.txFrames   = []; 
    this.rxFrames   = {};
    this.akFrames  = []; 

    this.txAckRequests   = 0;  // Keep track of dups 
    
    this.rxStart    = 0; // First winId of expected rx Window 
    this.rxAcked    = false; //
    this.rxSize     = 0;
   
/* 
    setInterval(()=>{
        // if not in active send, send ping (reset if we are not in SENDNEW)
        if ($this.txState != ARQ_SENDNEW ){
	    console.log("inactive timer ping");
            $this.ping(0x1);
	}
    },$this.options.pingTimeout);
*/
    
    const time = function(){
        return Date.now();
    };

    const resetTimer = function(){
        
        if (xutils.activeTimer($this.timeout)) // not var timeout used for idle state
            clearTimeout($this.timeout); // clear the old timer

        $this.timeout = setTimeout(()=> {
            logger.warn("ARQ Reset Timeout");
            $this.emit('error',"Reset Timeout");
        }, $this.options.resetTimeout | ARQ_TIMEOUT );
    }
    

    const sendDown =  function (dgram,length){
        dgram.setFlag($this.flags);
    
        dgram.calcCRC8(length);

        logger.info(dgram.frame().slice(0,length));
        $this.emit('writePhysical',dgram.frame().slice(0,length));
    }

    const sendUp = function(data){
        logger.info("Sending Up Payload: " + data.length);
        $this.emit('writeNetwork',data);
    }
  
    var _readPhysical = function(data) {
        var rxGram = new RxGram(data,data.length); // replace with flag
        logger.info("readPhysical:",data.slice(0,7));            
        $this.emit('physical',rxGram);
        resetTimer();
	}; 

    this.on('readPhysical',_readPhysical);
    
    this.readySync = function () {
       return  txq.length < MAX_TXNQ_LENGTH && $this.txState == ARQ_SENDNEW;
    }
    
    this.writeSync = function (data) {
        timeout = time();
        var written = 0;
        for(var i=0; i< data.length && $this.readySync(); i++){
          txq.push(data[i]);
          written++;
        }
        return written;
    }

    this.on('readNetwork',$this.writeSync);

    var _network = function(data,end){
        $this.txFrames.push(new TxGram(data,end));
    };

    this.on('network',_network);
        
    this.reset = function (state) {    
        timeout = time();
        $this.txOffset   = 0;
        $this.txStart    = 0; // First frame Id of tx Window being sent
        $this.txState    = state;
        $this.txAcked    = false;
        $this.txSize     = 0;
        $this.txFrames   = []; 
        $this.rxFrames   = {};
        $this.akFrames   = []; 
        
        $this.rxStart    = 0; // First winId of expected rx Window 
        $this.rxAcked    = false; //
        $this.rxSize     = 0;
        $this.lastState  = -1;
    };
    
    this.sendControl = function (ctype,param,param2,data=[]) {

        var txGram = new TxGram(Buffer(data),true);
        txGram._setWidHi(F_CNTRL);
        txGram._setWidLo(ctype);
        if (param != undefined)
            txGram.setControlParam(param == undefined ? 0x0:param)
        if (param2 != undefined)
            txGram.setControlParam2(param2 == undefined ? 0x0:param2)
		
        sendDown(txGram,DG_HEADER+txGram.getDataLength() );
        };

    
    this.ping = function (param) {
        logger.log("Send PING! " , param);
      //  if (param)
        //    $this.reset(ARQ_UNPUNG);
        $this.sendControl(F_PING,param);
        clearTimeout($this._iPingTimer);
        if (!xutils.activeTimer($this._iPingTimer)) {
            $this._iPingTimer = setTimeout(() => {
                    $this.emit("error","PING Timeout");
            },$this.options.pingTimeout);
            
        }
    };

    this.pong = function (param) {
        logger.log("Send PUNG! " + param);
	    var msSinceMidnight = new Date()-new Date().setHours(0,0,0,0);

	    var data = new Buffer(4);
	    data.writeUInt32BE(msSinceMidnight);
        $this.sendControl(F_PONG,param,undefined,data);
    };

    this.resetTimout = function () { // Called on ping request
        if ($this.options.resetTimeout && $this.options.resetTimeout ==-1)
               return;  
    };
    
    this.showState = function () {
        if ($this.txState == $this.lastState)
            return;
        $this.lastState = $this.txState;
        switch($this.txState ){
            case ARQ_UNPUNG:
                logger.info("State: ARQ_UNPUNG");
                break;
            case ARQ_SENDNEW:
                logger.info("State: ARQ_SENDNEW");
                break;
            case ARQ_REQACK:
                logger.info("State: ARQ_REQACK");
                break;
            case ARQ_RESEND:
                logger.info("State: ARQ_RESEND");
                break;
            case ARQ_ACKED:
                logger.info("State: ARQ_ACKED");
          } 
    };


   
    var _loop = function() {

        $this.showState();
        switch($this.txState){
            case ARQ_UNPUNG:
		console.log("ARQ_UNPUNG Send ping");
                $this.ping(0x1);
                break;
 
 
            case ARQ_SENDNEW:
                if ($this.txFrames.hasOwnProperty($this.txOffset)
                    && $this.txFrames[$this.txOffset].isSent() == false) {
                    
                    var ack = $this.txFrames[$this.txOffset].isEnd() || $this.txOffset == ARQ_WMAX -1;  
                    $this.txFrames[$this.txOffset].setMask($this.txOffset,ack);
                    logger.log("Mask " + $this.txFrames[$this.txOffset].getMask());
                    $this.txFrames[$this.txOffset].setWinId($this.txStart);
                    $this.txFrames[$this.txOffset].setAttempt($this.txFrames[$this.txOffset].attempts());
                    
                    if ( $this.akFrames.length ){
                            $this.txFrames[$this.txOffset].mergeAcked($this.akFrames.shift());
                    }

                    sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                    $this.txSize =  $this.txOffset + 1;
                    logger.log("Sending Down: ",DG_PAYLOAD);

                    if (ack){
                        $this.txState = ARQ_REQACK;
                        $this.txAckRequests   = 0;  // Keep track of dups 

                    } else {
                        $this.txOffset = $this.incrOffset($this.txOffset);
                    }
                } else if ( $this.akFrames.length ){
                    logger.log("Sending ACK");
                    sendDown($this.akFrames.shift(),DG_HEADER);
                }

                break;
            
            case ARQ_REQACK:
                // Repeat Ack request
                if ($this.txFrames.hasOwnProperty($this.txOffset) && $this.txFrames[$this.txOffset].ackRequested()){
                    var len = $this.txFrames[$this.txOffset].getDataLength();
                    if ( $this.akFrames.length ) {
                            $this.txFrames[$this.txOffset].mergeAcked($this.akFrames.shift());
                            sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                            $this.txFrames[$this.txOffset].unmergeAcked();
                            logger.error("Ack requestingx: " + $this.txFrames[$this.txOffset].attempts());
                            $this.txFrames[$this.txOffset].incrAttempts();
                    	    $this.txFrames[$this.txOffset].setAttempt($this.txFrames[$this.txOffset].attempts());
                            $this.txAckRequests = $this.txFrames[$this.txOffset].attempts();
			        } else {
                        logger.error("Ack requesting: ",$this.txOffset,  $this.txFrames[$this.txOffset].attempts());
                        $this.txFrames[$this.txOffset].incrAttempts();
                        $this.txAckRequests = $this.txFrames[$this.txOffset].attempts();
                        $this.txFrames[$this.txOffset].setAttempt($this.txFrames[$this.txOffset].attempts());
                        sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                        }
                $this.txFrames[$this.txOffset].setDataLength(len);
                } else if ( $this.akFrames.length ){
                    sendDown($this.akFrames.shift(),DG_HEADER);
                } else {
                    logger.error("Frame not found " + $this.txOffset);
                }
                break;
            
            case ARQ_RESEND:
                var sent=false;
                while(!sent){
                    logger.log("Trying to resend",$this.txOffset);
                    if ($this.txFrames.hasOwnProperty($this.txOffset)
                        && $this.txFrames[$this.txOffset]._acked == false){
                            logger.log("Resending",$this.txOffset);

                            sent=true;
                            if ( $this.akFrames.length >0 ){
                                logger.info("Merging Ack with TX");
                                $this.txFrames[$this.txOffset].mergeAcked($this.akFrames.shift());
                                sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                                $this.txFrames[$this.txOffset].unmergeAcked();
                            } else {
                                sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                            }

                        if ($this.txFrames[$this.txOffset].ackRequested()){
                            $this.txState = ARQ_REQACK;
                        }
                    }
                    if ($this.txState == ARQ_RESEND)
                        $this.txOffset = $this.incrOffset($this.txOffset);
                }
                
                if ( !sent && $this.akFrames.length ){
                    sendDown($this.akFrames.shift(),DG_HEADER);
                }
                break;

            case ARQ_ACKED:
                logger.info("Acked: Window Sent!");
                if ( $this.akFrames.length ) { 
                    sendDown($this.akFrames.shift(),DG_HEADER);
                }
                $this.txFrames.splice(0,$this.txSize); // purge head of queue
                $this.txState = ARQ_SENDNEW;
                $this.txStart = $this.incr($this.txStart,$this.txSize);   
                $this.txSize  = 0;
                $this.txOffset = 0;
                $this.txAcked = false;
                break;
        }
        
        // Check max attempts?
        
       // $this.showState();
    };
    

    this.on('loop',_loop);

    var _physical = function(rxGram){
        // Check checksum!
        
        if (!(rxGram.valid(DG_HEADER) || rxGram.valid(DG_SIZE))){
            logger.warn("Invalid frame - ignoring");
            return;
        }
        
        logger.info("Is ControlFrame :" + rxGram.isControlFrame());
        if ( rxGram.isControlFrame() && rxGram._getWidLo() == F_PING ) {
	    $this.showState();
            if (rxGram.getControlParam() == ARQ_RESET || $this.txState == ARQ_UNPUNG){
                logger.log("Resetting",rxGram.getControlParam());
		var reset = $this.txState == ARQ_UNPUNG ? ARQ_RESET:0;
                $this.reset(ARQ_SENDNEW);
                $this.pong( reset );   
                $this.resetTimout();
            } else{
                $this.pong(0);
                $this.resetTimout();
            }
            return;
        }

        if ( rxGram.isControlFrame() && rxGram._getWidLo() == F_PONG ) {
            logger.log("PONGED!");
            clearTimeout($this._iPingTimer);
	    if (rxGram.getControlParam() == ARQ_RESET || $this.txState == ARQ_UNPUNG)
            	$this.reset(ARQ_SENDNEW);
            return;
        }

        /*if ( $this.txState == ARQ_UNPUNG ) {
            $this.ping(0x1);// remove?
            return;
        }*/
        
        var txGram = $this.txFrames[$this.txOffset];

        if ( rxGram.ackReceived() ){
            // Handle Acknowledgements
            logger.info("ack Requested :" + $this.txSize);
            logger.info("txSize :" + $this.txSize);
            if ($this.txState == ARQ_REQACK){
                var acked = true;

                if (txGram.getWinId() !=  rxGram.getAckedWinId()){
                    logger.error("Received ack for wrong window");
                    acked = false;
                } else {
                    for(var i=0;i<$this.txSize; i++){
                      $this.txFrames[i]._acked = rxGram.isAckReceived(i);
                      logger.info(i + " : " + $this.txFrames[i]._acked);
                      if (acked &! $this.txFrames[i]._acked){
                        // request ack for last unacked frame frame
                        // and recalculate CRC
                        $this.txFrames[i].setMask(i,true);
                        acked = false;
                        $this.txOffset = i;
                      } 
                    }
                }
                logger.log("Window " + $this.txStart + " - Acked " + acked,"txOffSet",$this.txOffset);
                $this.txState = (acked) ? ARQ_ACKED:ARQ_RESEND;
                // Experimental
            } else {
                logger.log( rxGram.getAckedWinId(),"Already acked");
            }
        }
        

        if ( rxGram.hasData() ){
           // Handle data
           
            var offset   = rxGram.getWinOffset();
            logger.log("Received: " + rxGram.getWinId() + " offset: " +  rxGram.getWinOffset() );
            logger.info(util.inspect(rxGram.frame()));
          if ( $this.rxStart == rxGram.getWinId() ) { // Receviving current frame
                if (($this.rxFrames.hasOwnProperty(offset) == false) || 
                     ($this.rxFrames.hasOwnProperty(offset) && $this.rxFrames[offset].isRecv() == false)){
                    logger.info("Add frame at offset: " + offset);
                    $this.rxFrames[offset] = rxGram;
                    $this.rxFrames[offset].setRecv(true);
                } else {
                    logger.log("Frame already received: Sent Status:" ,$this.rxFrames[offset].isSent());
                }
            } else if ( $this.rxAcked // New rx window started
                && $this.inRange(rxGram.getWinId(), $this.incr($this.rxStart,$this.rxSize),
                                        $this.incr($this.incr($this.rxStart,$this.rxSize),ARQ_WMAX)                 
                                )) {
                // Handle new frame
                    logger.log("Create new frame set");
                    for(var i=0; i< ARQ_WMAX;i++){
                        if ($this.rxFrames.hasOwnProperty(i))
                            delete $this.rxFrames[i];
                        $this.rxFrames[i] = new RxGram(Buffer([]));
                        $this.rxFrames[i].setRecv(false);
                        $this.rxFrames[i].setSent(false);
                    }
                    // Create first frames
                    $this.rxFrames[offset] = rxGram;
                    $this.rxFrames[offset].setRecv(true);
                    $this.rxStart          = rxGram.getWinId();
                    $this.rxSize           = 0;
                    $this.rxAcked          = false;
            } else {
                logger.error("Receiving unexpected frame - window Id: " + rxGram.getWinId() + " expected " + $this.rxStart);
            }
        }

        // Send up all sequentially received windows
        for(var i=1; i< ARQ_WMAX; i++){
            if (i == 1 // send first frame
                && $this.rxFrames.hasOwnProperty(0)
                && $this.rxFrames[0].isRecv()
                && $this.rxFrames[0].isSent() == false){
                    logger.log("Sending up ",0);
                    sendUp($this.rxFrames[0].payload());
                    $this.rxFrames[0].setSent(true); // Change sent to function
                } else {
                   ;// logger.log("Unable to send up 0");
                }
        
                
            if ( $this.rxFrames.hasOwnProperty(i) 
                && $this.rxFrames.hasOwnProperty(i-1) 
                && $this.rxFrames[i].isRecv()
                && $this.rxFrames[i-1].isRecv()) {
                if ( $this.rxFrames[i-1].isSent()
                    && $this.rxFrames[i].isSent() == false){
                    logger.log("Sending up ",i);
                    sendUp($this.rxFrames[i].payload());
                    $this.rxFrames[i].setSent(true); // Change sent to function
                } else {
                    ;
                }
            } else {
              ;
                break;
            }
        }
            // Handle Ack requests

        if ( rxGram.ackRequested()  ) {
            logger.info("Ack  Requested" );
            logger.info("rxAcked " + $this.rxAcked )
            logger.info("rxStart " + $this.rxStart )
            logger.info("rxSize " + $this.rxSize  )

            offset   = rxGram.getWinOffset();
            $this.rxSize = ($this.rxSize == 0) ? (offset+1) : $this.rxSize;
            txGram = new TxGram(new Buffer([]),false);
            
            var acked =true;    
            for(var i=0; i < $this.rxSize; i++){
                if ($this.rxFrames.hasOwnProperty(i) && $this.rxFrames[i].isRecv() ){ // No need to check for sent 
                        txGram.setAckedMask(i,true); // check FrameIds!
                        logger.info("set Ack mask for " + (i));
                } else {
                        acked = false;
                       // logger.info("This rxFrames exists?",i,$this.rxFrames[i].isRecv());
                        logger.info("unset Ack mask for " + (i));
                }
            }

            $this.rxAcked = acked;
            txGram.setAckedWinId(rxGram.getWinId());
            txGram.setDataLength(0);
            if ($this.akFrames.length > 0){
                var anAck = $this.akFrames.peek();
                if (anAck.getAckedMask() != txGram.getAckedMask())
                   $this.akFrames.push(txGram);
            } else {
                $this.akFrames.push(txGram);
            }
        }
        
    };

	
    this.on('physical',_physical);

    var looping = true;
	
    this.loopInterval =setInterval(function(){
        var idle = ((time() - timeout) >  ARQ_IDLE)  
        var len;
        if ($this.txState == ARQ_SENDNEW && (txq.length > DG_PAYLOAD || txq.length > 0 && idle)){
            len = parseInt(Math.min(txq.length,DG_PAYLOAD-1)) ;
            $this.emit('network',new Buffer( txq.splice(0,len),'utf8'),txq.length == 0);
        }
    
        if (looping){
            looping = false;
            setTimeout(function(){
                $this.emit('loop');
                //logger.log(new Date(),"loop",$this.loopTimer(),$this.txAckRequests);
                looping = true;

            },$this.loopTimer())
        }
    },100);

   this.loopTimer = function() {

     if ( $this.txAckRequests >= ARQ_MAXRETRY ) {  // or Timeout since PING > opts.timeout
        logger.log("Max Retry - exiting");
        $this.emit("error","MAX Retry!");
        return; 
     }

	switch($this.txState) {
		case ARQ_UNPUNG:
			return $this.tickInterval / 2; // Modify to use average of txAckRequests as function
		case ARQ_REQACK:
		case ARQ_RESEND:
			    //return $this.tickInterval * Math.pow(2,$this.txAckRequests *0.5);
			    return $this.tickInterval;
		case ARQ_SENDNEW:
                return (this.txOffset == (ARQ_WMAX-1)) ? $this.tickInterval * 5: $this.tickInterval;
			    //return $this.tickInterval;
		default:
			return $this.tickInterval;
	    }
    };	

	this.end = function(){
    clearTimeout($this.loopInterval);
    clearTimeout($this.timeout);
    $this.removeListener('readPhysical',_readPhysical);
    $this.removeListener('readNetwork',$this.writeSync);
    $this.removeListener('network',_network);
    $this.removeListener('loop',_loop);
    $this.removeListener('physical',_physical);
	};

}       
   
     
ARQ.prototype.incr = function (x,y){
    return (x + y) % (ARQ_MAX);
}

ARQ.prototype.decr = function (x,y){
    return (x - y) % (ARQ_MAX);
}

ARQ.prototype.inRange = function(a,x,y){

 if (x ==y)
    return a == x;
 return a >= x && a <= (y+(( x < y ) ? 0 : ARQ_MAX));
}

ARQ.prototype.incrOffset = function (x) {
    return (x + 1) % (ARQ_WMAX);
}


module.exports.ARQ = ARQ;
module.exports.RxGram = RxGram;

