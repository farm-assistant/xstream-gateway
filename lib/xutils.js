const dateFormat = require('dateformat');
const RED="\x1b[31m%s";
const GREEN="\x1b[32m%s";
const CYAN="\x1b[36m%s";
const BLUE="\x1b[34m%s";


const LEVEL = {ERROR:0,WARN:1,LOG:2,INFO:3};
const _format = "MM:ss:l";
var _level =0;
var _circus = false;

var oldLog = console.log;
var oldWarn = console.warn;
var oldError = console.error;
var oldInfo = console.info;

console.log = function(){
    if (_level < LEVEL.LOG)
	return;
    if (_circus){
        Array.prototype.unshift.call(arguments,"[" + dateFormat(_format) +"] ");
        Array.prototype.unshift.call(arguments, CYAN);
    }
    oldLog.apply(this, arguments);
}

console.warn = function(){
    if (_level < LEVEL.WARN)
    	return;
    if (_circus){
        Array.prototype.unshift.call(arguments,"[" + dateFormat(_format) +"] ");
        Array.prototype.unshift.call(arguments, BLUE);
    }
    oldWarn.apply(this, arguments);
}


console.error = function(){
    if (_level < LEVEL.ERROR)
    	return;
    if (_circus){
        Array.prototype.unshift.call(arguments,"[" + dateFormat(_format) +"] ");
        Array.prototype.unshift.call(arguments, RED);
    }
    oldError.apply(this, arguments);
}

console.debug  = function(){
     
    if (_level < LEVEL.INFO)
    	return;
    if (_circus){
        Array.prototype.unshift.call(arguments,"[" + dateFormat(_format) +"] ");
    }
    oldInfo.apply(this, arguments);
}

console.info = function(){
    if (_level < LEVEL.INFO)
    	return;
    if (_circus){
        Array.prototype.unshift.call(arguments,"[" + dateFormat(_format) +"] ");
        Array.prototype.unshift.call(arguments, GREEN);
    }
    oldInfo.apply(this, arguments);
}



exports.logger = {
    log: console.log,
    warn: console.warn,
    error: console.error,
    info: console.info,
    debug: console.info
};

exports.logx = function(x,format=false){
  _level = x;
  _circus = format;
   console.log("setting log level",x);
  return this.logger;
};

exports.activeTimer = function(timer){
    return (timer
        && timer.hasOwnProperty("_onTimeout")
        && timer._onTimeout != null);
};
