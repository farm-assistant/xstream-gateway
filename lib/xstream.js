"use strict";

const xutils = require("./xutils.js"); 
const logger = require("./xutils.js").logger; // INFO/DEBUG
const Events = require('events').EventEmitter;
const util = require('util');
const xbee_api = require('xbee-api');
const C = xbee_api.constants;
const fs = require("fs");


function xBeeStream(xbeeAPI,serialport){

    Events.call(this);
    this.xbeeAPI = xbeeAPI;
    this.serialport = serialport;
    var $this = this;

    this.serialport.on('open',function(){
       logger.info("Serial port opened: " + serialport.path); 
    });


   this.purgeSockets = function(dirPath, removeSelf = false) {
      logger.info("purging Sockets",dirPath);
      try { var files = fs.readdirSync(dirPath); }
      catch(e) { return; }

      if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
          var filePath = (dirPath + '/' + files[i]).toString();
	  logger.log(filePath);
	  try {
           fs.unlinkSync(filePath);
	  } catch(e){ logger.error(e); continue;};
        }
      if (removeSelf)
        fs.rmdirSync(dirPath);
    };



    this.localATCmd  = function (cmd,param =[]) {
        try{
            var frame =  {
                id: 1,
                type:C.FRAME_TYPE.AT_COMMAND,
                command: cmd,
                commandParameter: param // Can either be string or byte array. 
            };
            logger.info("Sending Local AT CMD",frame);
            $this.serialport.write(xbeeAPI.buildFrame(frame),function(err, res) {
                if (err)
                    throw(err);
              });
        } finally{
            ;
        };
    }


    this.remoteATCmd  = function (cmd,dest) {
        try{
            var frame =  {
                id: 1,
                type:C.FRAME_TYPE.REMOTE_AT_COMMAND_REQUEST,
                destination64: dest,
                command: cmd,
                commandParameter: [] // Can either be string or byte array. 
            };
            logger.info("Sending Remote AT CMD",frame);
            $this.serialport.write(xbeeAPI.buildFrame(frame),function(err, res) {
                if (err)
                    throw(err);
              });
        } finally{
            ;
        };
    }

   
    // Broadcast rset control (or specify dest addr)

    this.rset = function (dest='000000000000FFFF') {
        // checksum, restart firmware,0 args, void,not client,0 len data	
        this.write(new Buffer([0xE4,0xf5, 0x00, 0x00, 0x00, 0x00]), dest, 1);	
    }

    // Simple helo.  Rset not required. Let end nodes update dest addr
    this.helo= function (dest='000000000000FFFF') {
        // checksum, ping req, reset arq,void,not client,0 data	
        this.write(new Buffer([0x86, 0xf4, 0x00, 0x00, 0x00, 0x00]), dest, 0);	
    }

    
    // Broadcast ping (or specify dest addr)

    this.ping= function (dest='000000000000FFFF') {
        // checksum, ping req, reset arq,void,not client,0 data	
        this.write(new Buffer([0x7d, 0xf1, 0x01, 0x00, 0x00, 0x00]),	dest, 1);	
    }

    
    this.write = function (data,dest,id=0) {
        try{
            var frame = {
              id: 0, // We don't want Zigbee level acks
              type: C.FRAME_TYPE.TX_REQUEST_64,
              destination64: dest,
              options: 0x01, // optional, 0x00 is default
              data: data // Can either be string or byte array.
            };
            logger.info(util.inspect(data));
            $this.serialport.write(xbeeAPI.buildFrame(frame),function(err, res) {
                if (err)
                    throw(err);
              });
        } finally{
            ;
        }
    };
}

xBeeStream.prototype = Object.create(Events.prototype);
module.exports.xBeeStream = xBeeStream;

