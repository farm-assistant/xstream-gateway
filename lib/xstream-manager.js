"use strict";

const xutils = require("./xutils.js"); 
const logger = require("./xutils.js").logger; // INFO/DEBUG
const Events = require('events').EventEmitter;
const net = require('net');
const fs = require('fs');
const stream = require('stream');
const encode = require("simplepacket").encode;
const decode = require("simplepacket").decode;


function createSocketsFolder(path, mask, cb) {
    if (typeof mask == 'function') { // allow the `mask` parameter to be optional
        cb = mask;
        mask = '0777';
    }
    fs.mkdir(path, mask, function(err) {
        if (err) {
            if (err.code == 'EEXIST') cb(null); // ignore the error if the folder already exists
            else cb(err); // something else went wrong
        } else cb(null); // successfully created folder
    });
}


function xStreamManager(xstream, xarq, address64, options) {

    Events.call(this);
    this.xstream = xstream;
    this.options = options;
    this.path = (options.socketPath + "/" + address64.toString()).toString();
    this.address64 = address64;
    this.arq = xarq;
    this.BEG = 0x12;
    this.END = 0x13;
    this.DLE = 0x7D;
    this.TIMEOUT = 100; // 256ms? Socket Timeout
    var $this = this;
    
    var pdus = [];

    createSocketsFolder(options.socketPath, '0744', function(err) {
        if (err) {
            logger.error(err);
            process.exit(1);
        }
    });


    this.arq.readPhysical = function(data) {
        $this.arq.emit('readPhysical', data);
    };

    this.xstream.on(this.address64, this.arq.readPhysical);

    this.arq.on('writePhysical', (data) => {
        logger.info("writePhysical: ", $this.address64);
        $this.xstream.write(data, $this.address64);
    });

    this.link = function(nodeIdentifier) {
        if (!nodeIdentifier.length) // check for valid filename
            return;
        $this.linkPath = ($this.options.socketPath + "/" + nodeIdentifier).toString();
        if (!fs.existsSync($this.linkPath)) {
            fs.symlinkSync($this.path, $this.linkPath);
        }
    };

    this._cleanup = function() {
        logger.warn("Tearing down stream for: " + $this.address64);
	pdus = [];
        clearTimeout($this._iExitSend);
        clearTimeout($this._iPingTimer );
        clearInterval($this._iBt); // Clear timers
        clearInterval($this._iCt);
        if (!this.arq)
            return;
        $this.arq.end();
        if (this.xstream)
            $this.xstream.removeListener($this.address64, $this.arq.readPhysical);
        $this.server.close(); // 
        logger.info("Delete paths");
        $this.deletePath(); // Get rid of stale socket and link
        delete $this.server; // delete object
        delete $this.arq;
        if (this.xstream)
            $this.xstream.helo($this.address64);
        $this.emit('end'); // Tell parent we're ending
    };

    this.arq.on('end', () => {
        $this._cleanup();
    });

    this.arq.on('error', (e) => {
        logger.warn("Error received on: " + $this.address64,e);
        $this._cleanup();
    });
    

    //    logger.log("Trying to create listener for " + $this.path);
    this.server = net.createServer(function(socket) {
        $this.socket = socket;
        $this.socket.connected = true;
        logger.log("Connection established: " + $this.path);

        var _write = function(data) {
            logger.info("_write: " + socket.writable);
            logger.info(data,socket !="undefined");
            if (socket && socket.writable) $this.obufferStream.push(data);
        }


        // Input:
        // Two layers of buffering
        // Buffer ibufferStream from Ux socket
        // Flush on timeout
        // Swap corks socket=>buffer after flush
        // recork ibuffer when flushed
        // uncorksocket when writeSync completed
        socket.setTimeout($this.TIMEOUT);
       // socket.setKeepAlive(true, $this.TIMEOUT);

        $this.ibufferStream = (options.buffered) ? encode() : new stream.PassThrough();
        $this.ibufferStream.cork();

        $this.obufferStream = new stream.PassThrough();
        if (options.buffered) {
            var dc = decode();
            var buffer = Buffer(0);
            $this.obufferStream.pipe(dc, {
                end: false
            });
            dc.on('data', (data) => {
                buffer = Buffer.concat([buffer, data]);
            });

            dc.on("flush", () => {
        	socket.setTimeout($this.TIMEOUT);
                logger.info("Flush");
                logger.info(buffer);
                var corked = socket._writableState.corked;
		if ($this.socket.connected){
			logger.log("xWriting",buffer.length);
               		$this.socket.write(buffer);
		}
                if (corked) socket.uncork();
                if (corked) socket.cork();
                buffer = Buffer(0);
            });
        } else {
            $this.obufferStream.pipe(socket, {
                end: false
            });
        }
        
        var _queuedSend = function () {

            const xutils = require("./xutils.js"); 

            if (xutils.activeTimer($this._iBt))
                return;
                
            if (pdus.length == 0)
                return;

            if (!($this.hasOwnProperty("arq") && $this.arq && $this.arq.readySync()))
                return;
            
            var d =  pdus.shift();

            $this._iExitSend = setTimeout(() =>{
               $this.arq.emit("error","Error: Sending timeout");
            },$this.options.sendTimeout | 30000);

            $this._iBt = setInterval(() => {
               // Check to ensure if ready before sending!
                // Todo: Increase counter and exit if
                // wait too long!
                
                var written = $this.arq.writeSync(d);

                if (written)
                    logger.log("Written:", written,"/",d.length);
                // repeat until written

                if (written < d.length) {
                    d = (written) ? d.slice(written) : d;
                } else {
                    // iBuffer corked, socket uncorked
                    console.log("Send complete");
                    clearInterval($this._iBt);
                    clearTimeout($this._iExitSend);
                    socket.uncork();
               }
            }, 200);
        }

        setInterval(() =>{
            // Not sending - avoid race condition
            //console.log("PDU Size",pdus.length);
            _queuedSend();
        },1000);

        $this.ibufferStream.on('data', (d) => {
            pdus.push(d);
        });


        socket.on('timeout', () => {
            logger.log("socket on timeout");
            // Ensure we have something to write
            if ($this.ibufferStream._writableState.length == 0 || $this.ibufferStream._writableState.corked == false)
                return;
            // Wait for arq ready before unokign
            // If time out then disconnect before sending!!!@

            // iBuffer uncorked, socket corked
            logger.info("uncorking");
            socket.cork();
            // Add possible time out here? Or better still wait for arq.SENDNED!!!
            $this.ibufferStream.uncork();
            $this._iCtStatus =0;

            $this._iCt = setInterval(() => {
                
                // If encoding, wait until iBuffer empty
                // before flushing out buffer as encoded
                if ($this.ibufferStream._writableState.length > 0)
                    return;
                clearInterval($this._iCt);
                if (options.buffered)
                    $this.ibufferStream.flush();

                // Replace the cork, but wait until iBuffer has been
                // flushed out with arq.writeSync before uncorking socket
                logger.info("corking");
                $this.ibufferStream.cork();
            }, 200)
        });


        var _read = function(data) {
            // Check if corked?
            //        if ($this.ibufferStream._writableState.corked)
            //          return; // Log error - buffer overflow!
            $this.ibufferStream.write(data);
        };

        $this.arq.on('writeNetwork', _write); // write data to UNIX socket from network
        socket.on('data', _read); // read data from UNIX socket
        socket.on('connect', () => {
            // Output
        });


        socket.on('end', () => {
            logger.log("Connection disconnected: " + $this.path);
	    $this.socket.connected = false;
 //           $this.arq.ping(0x0);
            $this.socket.connected = false;
            clearInterval($this._iBt); // Clear timers
            clearInterval($this._iCt);
            pdus = [];
            $this.socket.uncork();
            delete $this.obufferStream; 
            delete $this.ibufferStream;
            if ($this.arq && _write !== undefined){
                $this.arq.removeListener('writeNetwork', _write);
            }
        });
        socket.on('error', (e) => {
            logger.error("Socket error: ", $this.path,e);
	    $this._cleanup(e);
        });
    });

    this.deletePath = function() {
        if ($this.linkPath) {
            logger.info("Deleting existing link", $this.linkPath);
            fs.unlink($this.linkPath, (e) => {});
        }

        if (fs.existsSync($this.path)) {
            logger.info("Deleting existing socket", $this.path);
            fs.unlinkSync($this.path);
        }
    };

    this.deletePath(); // Clean up old paths
    this.server.listen($this.path, function() {
       fs.chmodSync($this.path, '777');
       logger.log('Server listening on ' + $this.path);
    });

    this.server.on('error', function(e) {
        logger.log("Error : " + e.toString());
	process.exit(0);
    });
    this.server.on('end', function() {
        $this.deletePath();
    });

    this.close = function() {
        $this.server.close();
    }
}


xStreamManager.prototype = Object.create(Events.prototype);
module.exports.xStreamManager = xStreamManager;
