var FC = require('./node-modbus-stack/modbus-stack').FUNCTION_CODES;

var handlers = {};

handlers[FC.READ_COILS] = function(request, response) {
  response.writeException(2);
}

handlers[FC.READ_HOLDING_REGISTERS] = function(request, response) {
  console.log(request);
  setTimeout(function() {
  	response.writeResponse(new Array(request.quantity));
  }, 800);
}

handlers[FC.READ_INPUT_REGISTERS] = function(request, response) {
  console.log(request);
 var start = request.startAddress;
  var length = request.quantity;

  var resp = new Array(length);
  for (var i=0; i<length; i++) {
    resp[i] = start + i;
  }
  response.writeResponse(resp);

}

require('./node-modbus-stack/server').createServer(handlers).listen(1502);
