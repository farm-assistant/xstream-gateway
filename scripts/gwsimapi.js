#!/usr/bin/env node

'use strict';

const net = require("net");
const fs = require("fs");
const path = require("path");
const _ = require("lodash");
const api = require("../").API();
const xgw  = require("../");
const msgpack = require("msgpack5")();

const maxSocks =10;
const sockets_path = '/tmp/sockets';
const image_path = __dirname + "/../images";
const toggle = {}; // Default value of toggle switches
const tank = {};
const toggled = {};
const tankEmptySeconds = 3600 *6;
const tankFillSeconds  = 3600 *2;
const gpsdefault = [-46.0848556,167.7720027,50];
const  GPS = {};
const _IMAGES = [];

Array.prototype.rand = function(){
 return this.length ? this[Math.round(Math.random() * this.length-1)] : null
}


var files = fs.readdirSync(image_path);

for(var i in files) {
    var img = files[i];
    if ( img.toString().indexOf(".jpg") > 0 ){
        _IMAGES.push(image_path + "/" + img.toString());
    }
}

console.log(_IMAGES.length.toString() + " images loaded");

const simulateAnalog = function(socket,iface){
    const dt = new Date();
    /*const seconds = dt.getSeconds() + (60 * (dt.getMinutes() + (60 * dt.getHours())));
    const value = 50 * (1 + Math.sin(seconds / 10000));
    */
    console.log(iface.urn());
    console.log("Tank before:" + tank[iface.urn()])
    console.log("Toggle:" + toggle[iface.urn()])
    if (toggle[iface.urn()]) { // pump running
        tank[iface.urn()]  =  Math.min(tank[iface.urn()] + ((0.00001/tankFillSeconds) *  (dt - toggled[iface.urn()])),1);
    } else {
        tank[iface.urn()]  =  Math.max(tank[iface.urn()] - ((0.00001/tankEmptySeconds) *  (dt - toggled[iface.urn()])),0);
    }

//    console.log("Tank after:" + tank[iface.urn()])
    var senML = api.makeSenML({bt:"urn:dev:xbee:" + socket.getsockname(),
                               pos:GPS[iface.urn()]},
                               [{n:iface.name(),v:tank[iface.urn()]*100}]);
    console.log(senML);
    return senML;
};

const simulateCamera = function(socket,iface){

		  var img = _IMAGES.rand();
    
    console.log('Sending ' + img);
    var senML = api.makeSenML({bt:"urn:dev:xbee:" + socket.getsockname(),
                                pos:GPS[iface.urn()]},
                                [{n:iface.name(),vd:fs.readFileSync(img)}]);
    return senML;
};


// Simulate a toggle switch


const simulateRelay = function(socket,iface){

    console.log(iface.value());
    
    var _toggle;
    if (typeof(iface.value()) == 'boolean') {
        _toggle = iface.value();
        toggled[iface.urn()] = toggled[iface.urn()] != _toggle ? new Date() :  toggled[iface.urn()];
        toggle[iface.urn()] = _toggle;
    } else {
       _toggle  = toggle[iface.urn()];
    }
    console.log("Toggle now: " + toggle[iface.urn()]);

    var senML = api.makeSenML({bt:"urn:dev:xbee:" + socket.getsockname(),
                                pos:GPS[iface.urn()]},
                                [{n:iface.name(),v:_toggle}]);
    return senML;
};


const requestValue = function(socket, iface) {
    // all real device communication should be done here.
    // since this is async method we can safely take our time making
    // serialization and related processing.
    // it's ok if this method does nothing, if for ex. device is unavailable.
    // leaving all serialization below this method allows to leave
    // xstream/msgpack in here.
    
    setTimeout(() => { // simulating real-world communication
        var senML ={};
            switch(iface.name()){
                case 'AD0':
                case 'AD1':
                case 'AD2':
                case 'AD3':
                    senML = simulateAnalog(socket,iface);
                    break;
                case 'CAM':
                    senML = simulateCamera(socket,iface);
                    break;
                case 'DI0':
                    senML = simulateRelay(socket,iface);
                    break;
                default:
                    console.error("Unkown interface " + iface);
                    return;
            }
            
            socket.writer.write(msgpack.encode(senML));
            socket.writer.flush();
        }, 1000);
}



function gatewaySimulator() {
    
    if (!fs.existsSync(sockets_path)){
        fs.mkdirSync(sockets_path);
    }
        
    for (let i of _.range(maxSocks)) {
        var sockName = `${(0xFFFF0000 + i).toString(16)}`
        const socketPath = path.join(sockets_path, sockName);
        toggle["urn:dev:xbee:" + sockName] = false; // toggle (pump) starts off
        tank["urn:dev:xbee:" + sockName] = 1; // tank starts full
        toggled["urn:dev:xbee:" + sockName] = new Date(); // Last Toggled
        GPS["urn:dev:xbee:" + sockName] = [gpsdefault[0] +  (Math.random()-0.5)/100,
                                           gpsdefault[1] +  (Math.random()-0.5)/100,
                                           gpsdefault[2]];

        if (fs.existsSync(socketPath)) {
            fs.unlinkSync(socketPath);
        }

        
        const device = net.createServer(function(socket) {
                socket.getsockname = function(){
                    return path.basename(this.server._pipeName);
                };
                socket.reader = xgw.decode();
                socket.writer = xgw.encode();
                socket.writer.pipe(socket);
                socket.pipe(socket.reader);
                var chunks = [];

                socket.reader.on('data',function(data){
                  chunks.push(data); 
                }).on('flush',function(){
                   const payload = msgpack.decode(Buffer.concat(chunks));
                   chunks = [];
                   console.log(payload);
                   if (!api.parseSenML(payload)){
                        console.log("Failed to parse SenML payload");
                       return;
                   }
     
                    for (let record of payload.items()) {
                        if (record.name().match(/(AD\d|DI\d|CAM)/)) {
                            requestValue(socket, record);
                        }
                    }
                });
        });
        device.listen(socketPath);
    }
}

process.on('SIGINT', function () {
    for (let i of _.range(maxSocks)) {
        var sockName = `${(0xFFFF0000 +i).toString(16)}`
        const socketPath = path.join(sockets_path, sockName);

        if (fs.existsSync(socketPath)) {
            fs.unlinkSync(socketPath);
        }
    }
    process.exit(0);
});



gatewaySimulator();
