// AgSense End node simulator
// Usage: node ./gatewaysim
// Creates a UNIX socket and responds to a set of simple queries of the form {q,"<query type>"} i.e. {q,"GPS"}
// See this.path for name of UNIX socket

var net = require('net');
var fs = require("fs");
var mp = require("msgpack");
var xst = require("../index.js");
var yaml = require("js-yaml");

var conf = yaml.safeLoad(fs.readFileSync("../config/config.yaml", 'utf8'));

Array.prototype.rand = function(){
 return this.length ? this[Math.round(Math.random() * this.length)] : null
}


function simpleUnix() {
    
    var $this = this;
    this.path = conf.socketPath + "/" + "0x123";	 // change as required.
    this.socket = undefined;
    this.server = net.createServer(function(socket) {
        if ($this.socket){
            $this.socket.emit('close');
        }
        console.log("Setting up");
        socket.on('close',function(){
           console.log("Socket closed");
           $this.socket = undefined;
           xdc.end();
        });
        xen = xst.encode();
        xdc = xst.decode();
        $this.socket = socket;
        
        $this.socket.pipe(xdc);
        xen.pipe($this.socket);
        var chunks = [];
        xdc.on('data',function(data){
          chunks.push(data); 
        }).on('flush',function(){
           var msg = mp.unpack(Buffer.concat(chunks));
           console.log(msg);

           xen.write(mp.pack($this.processMsg(msg)));
           xen.flush();
           chunks = [];
        });
    });
    
    this.deletePathAsync = function (){
        if (fs.existsSync($this.path)) {
            fs.unlinkSync($this.path);
        }
    };
     
    this.deletePathAsync();

    this.server.listen($this.path,function() {
        console.error('Server listening on ' + $this.path);
    });

    var toggle = true;
    this.processMsg = function(msg) { // Unpacked structure
        if (msg.hasOwnProperty('q')) {
            switch(msg.q) {
                case "GPS":
                    return {r:{GPS:[123,456]}};
                case "AD0":
                case "AD1":
                case "AD2":
                case "AD3":
                    var m = Math.round(Math.random() * 100);
                    var k ={};
                    k[msg.q] = m;
                    return {'r':k};
                default:
                    return {e:"Unknown Sensor"};
            }
        }
        return {e:"Unknown Command"};        
    }
}

simpleUnix();
